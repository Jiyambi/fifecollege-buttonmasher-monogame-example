﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio; // for sound effects
using Microsoft.Xna.Framework.Graphics; // For visuals
using Microsoft.Xna.Framework.Input; // For mouse clicks
using Microsoft.Xna.Framework.Media; // For music

namespace FifeCollege_ButtonMasher_MonoGame_Example
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Variables to be used in the game
        Texture2D buttonTexture;
        SpriteFont gameFont;

        // Game audio
        Song gameMusic;
        SoundEffect clickSFX;
        SoundEffect gameOverSFX;

        // Game State Tracking
        MouseState previousMouseState;
        bool playing = false;
        int score = 0;
        float timePassed = 0f;
        float timeLimit = 5f;
        float cooldown = 1f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here



            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load button graphic
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            // Load game font
            gameFont = Content.Load<SpriteFont>("fonts/mainSpriteFont");

            // Load game audio
            gameMusic = Content.Load<Song>("audio/music");
            clickSFX = Content.Load<SoundEffect>("audio/buttonclick");
            gameOverSFX = Content.Load<SoundEffect>("audio/gameOver");


            // Start background music
            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            // Set mouse to visible
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            MouseState mouseState = Mouse.GetState();

            // If the button was NOT pressed and now we've pressed it, register a click
            if (previousMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
            {
                // This was a click, but was it on the button?

                // NOTE: This is copied from draw, violates DRY (don't repeat yourself) principle!
                // We should move this to a funciton, or better yet, a class! (future :)
                // Figure out the center of the screen (useful for positioning)
                Vector2 screenCenter = new Vector2(
                    Window.ClientBounds.Width / 2,
                    Window.ClientBounds.Height / 2);
                Vector2 buttonCenter = new Vector2(
                    buttonTexture.Width / 2,
                    buttonTexture.Height / 2);
                Rectangle buttonRect = new Rectangle((int)screenCenter.X - buttonTexture.Width/2, (int)screenCenter.Y - buttonTexture.Height/2, buttonTexture.Width, buttonTexture.Height);

                if (buttonRect.Contains(mouseState.X, mouseState.Y))
                {
                    // Play a sound effect
                    clickSFX.Play();

                    // Change to playing if not already
                    if (playing == false)
                    {
                        // cooldown time for when you've just ended so you don't start again!
                        if (timePassed >= cooldown)
                        {
                            playing = true;

                            // Start timer
                            timePassed = 0f;

                            // Set score to 0
                            score = 0;
                        }
                    }
                    else // already playing, add score
                    {
                        //  Add score
                        ++score;
                    }
                }

            }

            // Timer incrementing
            timePassed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (playing == true)
            {
                // Handle running out of time!
                if (timePassed >= timeLimit)
                {
                    playing = false;
                    timePassed = 0f;
                    gameOverSFX.Play();
                }
            }
            
            previousMouseState = mouseState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            // Start drawing
            spriteBatch.Begin();

            // Figure out the center of the screen (useful for positioning)
            Vector2 screenCenter = new Vector2(
                Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2);

            // Draw button sprite
            Vector2 buttonCenter = new Vector2(
                buttonTexture.Width / 2,
                buttonTexture.Height / 2);
            Rectangle buttonRect = new Rectangle((int)screenCenter.X, (int)screenCenter.Y, buttonTexture.Width, buttonTexture.Height);
            spriteBatch.Draw(buttonTexture, // image to be drawn   
                buttonRect,                 // Location and size of image on screen
                null,                       // Source rect
                Color.White,                // Color tint for image
                0f,                         // Rotation of image
                buttonCenter,               // Anchor point of image (for position, rotation, and scaling)
                SpriteEffects.None,         // Sprite effects applied to image
                0);                         // Layer depth (what is drawn on top of what)


            // Draw Text

            Vector2 stringsize = gameFont.MeasureString("Button Masher");
            spriteBatch.DrawString(gameFont, "Button Masher", screenCenter + new Vector2(0,-150f) - stringsize/2.0f, Color.Black);
            stringsize = gameFont.MeasureString("by Sarah Herzog");
            spriteBatch.DrawString(gameFont, "by Sarah Herzog", screenCenter + new Vector2(0, -100f) - stringsize / 2.0f, Color.Black);

            string promptText = "Click the Button to start the game!";
            if (playing == true)
            {
                promptText = "Mash the button to get score!";
            }
            stringsize = gameFont.MeasureString(promptText);
            spriteBatch.DrawString(gameFont, promptText, screenCenter + new Vector2(0, -50f) - stringsize / 2.0f, Color.Black);
            
            spriteBatch.DrawString(gameFont, "Score:", new Vector2(10,10), Color.Black);
            spriteBatch.DrawString(gameFont, score.ToString(), new Vector2(100, 10), Color.Black);

            spriteBatch.DrawString(gameFont, "Timer:", new Vector2(Window.ClientBounds.Width - 150, 10), Color.Black);
            int timeAsInt = (int)timePassed;
            if (playing == false)
                timeAsInt = 0; // Don't display time if not playing
            spriteBatch.DrawString(gameFont, timeAsInt.ToString(), new Vector2(Window.ClientBounds.Width - 50, 10), Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
